def testSpreadOperator() {
    toCount = ["one", "two", "three"]
    expected = [3, 3, 5]
    actual = toCount*.length()
    assert actual == expected
}

def testRangeSum() {
    s = (1..10).sum()
    assert s == 55
}

def testClosure() {
    evens = (1..10).findAll { x -> x % 2 == 0 }
    assert evens == [2, 4, 6, 8, 10]
}

testSpreadOperator()
testRangeSum()
testClosure()